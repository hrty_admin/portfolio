;(function(window, document) {

	'use strict';


	var WorksApp = angular.module('WorksApp', [
		'ngRoute',
		'ngSanitize',
		'WorksServices',
		'WorksControllers',
		'WorksDirectives',
		'WorksFilters'
	]);

	WorksApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		$locationProvider.html5Mode(true);
		$routeProvider.
			when('/', {
				templateUrl: 'partials/works/list.html',
				controller: 'ListController'
			}).
			when('/works/others.html', {
				templateUrl: 'partials/works/other-list.html',
				controller: 'OtherListController'
			}).
			when('/works/:id', {
				templateUrl: 'partials/works/detail.html',
				controller: 'DetailController'
			}).
			otherwise({
				redirectTo: '/'
			});
	}]);



	/* ********************
	// service
	// ****************** */
	var WorksServices = angular.module('WorksServices', ['ngResource']);

	WorksServices.factory('Work', ['$resource', function($resource) {
		return $resource('json/works-list.json');
	}]);

	WorksServices.factory('OtherWork', ['$resource', function($resource) {
		return $resource('json/other-works-list.json');
	}]);

	// mixItUp 判定用
	WorksServices.value('IsMixing', {
		isMixing: null
	});


	/* ********************
	// controller
	// ****************** */
	var WorksControllers = angular.module('WorksControllers', []);

	WorksControllers.controller('ListController', ['$scope', '$timeout', 'Work', 'IsMixing',  function($scope, $timeout, Work, IsMixing) {
		window.scrollTo(0,0);

		$scope.works = [];

		Work.query(function(data, headers) {
			$scope.works = data;
		});

		$scope.$on('ngRepeatFinished', function() {
			var $workContainer = $('#workContainer'),
				$FilterSelect = $('#FilterSelect');

			if(IsMixing.isMixing){
				$workContainer.mixItUp('destroy');
				if(!isVisited) {
					$workContainer.mixItUp();
				}
			}

			if(isVisited) {
				$workContainer.mixItUp();
			}

			$FilterSelect.on('change', function(){
				$workContainer.mixItUp('filter', this.value);
			});

			IsMixing.isMixing = true;
		});

	}]);

	WorksControllers.controller('OtherListController', ['$scope', '$timeout', 'OtherWork',  function($scope, $timeout, OtherWork) {

		window.scrollTo(0,0);

		$scope.otherWorks = [];

		OtherWork.query(function(data, headers) {
			$scope.otherWorks = data;
		});

		$scope.$watch('filteredOtherWorks', function() {
			$scope.filtering = true;
			$timeout(function() {
				$scope.$emit('ngSortFinished');
			},100);
		}, true);

		$scope.$on('ngSortFinished', function() {
			$scope.filtering = false;
		});

	}]);

	WorksControllers.controller('DetailController', ['$scope', '$routeParams', 'Work',  function($scope, $routeParams, Work) {
		var workId = $routeParams.id;
		var works = [];

		$scope.work;
		$scope.currentWorkNumber;
		$scope.deviceImg;
		$scope.showImg = false;

		window.scrollTo(0,0);

		Work.query(function(data, headers) {
			works = data;
			for(var i = 0; i < data.length; i++) {
				if(workId == data[i].id) {
					$scope.work = data[i];
					$scope.currentWorkNumber = i;
					if($scope.work.imagePC[0]) {
						$scope.toggleDevices('pc');
					} else {
						$scope.toggleDevices('sp');
					}
					return;
				}
			}
		});

		$scope.getPrevId = function(n) {
			var _prevWork = works[n-1];
			if(!_prevWork) {
				return;
			} else {
				return _prevWork.id;
			}
		};

		$scope.getNextId = function(n) {
			var _nextWork = works[n+1];
			if(!_nextWork) {
				return;
			} else {
				return _nextWork.id;
			}
		};

		$scope.toggleDevices = function(d) {
			if(d == "pc") {
				$scope.deviceImg = "pc";
			} else {
				$scope.deviceImg = "sp";
			}
		};

		$scope.$on('ngRepeatFinished', function() {
			$('.work-carousel-pc').slick({dots: true});
			$('.work-carousel-sp').slick({dots: true});
			$scope.showImg = true;
		});

	}]);




	/* ********************
	// directive
	// ****************** */
	var WorksDirectives = angular.module('WorksDirectives', []);

	// list画像出力
	WorksDirectives.directive('ngSetDevicesInfo', [function() {
		return function (scope, element, attrs) {
			scope.devicesImage = {
				imagePC: scope.work.imagePC[0],
				imageSP: scope.work.imageSP[0]
			};
			if(scope.devicesImage.imagePC && scope.devicesImage.imageSP) {
				scope.devicesInfo = "pc-sp";
			} else if(scope.devicesImage.imagePC) {
				scope.devicesInfo = "pc";
			} else {
				scope.devicesInfo = "sp";
			}
		};
	}]);

	// item画像出力
	WorksDirectives.directive('ngRepeatWatch', ['$timeout', function($timeout) {
		return function (scope, element, attrs) {
			if(scope.$last) {
				$timeout(function() {
					scope.$emit('ngRepeatFinished');
				});
			}
		};
	}]);

	// scope class
	WorksDirectives.directive('ngScopeClass', [function() {
		return function (scope, element, attrs) {
			var $element = $(element),
				scopeArr = scope.work.scope,
				workingForm = scope.work.workingForm;

			for(var i = 0; i < scopeArr.length; i++) {
				var tgt = scopeArr[i].replace(/\s\/\s/g, "");
				$element.addClass(tgt);
			}
			$element.addClass(workingForm);
		};
	}]);





	/* ********************
	// filter
	// ****************** */
	var WorksFilters = angular.module('WorksFilters', []);

	WorksFilters.filter('filterd', ['$filter', function($filter) {
		return function(scopes, input) {
			var result = $filter('filter')(scopes, input);
			return result;
		}
	}]);

	// WorksFilters.filter('reverse', function() {
	// 	return function(scopes, input) {
	// 		if (!scopes || !scopes.length) { return; }
	// 		return scopes.slice().reverse()
	// 	}
	// });



})(this, this.document);