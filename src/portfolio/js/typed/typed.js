(function(window, $){
	"use strict";

	var isVisited = window.isVisited;

	var initializeElm,
		loadingWrap,
		typeLego;

	var typing = function() {
		$("#typing").typed({
			strings: ["INTRODUCE ^1200 MY ^500 WORKS."],
			typeSpeed: 50,
			callback: function() {
				setTimeout(function() {
					contentLoaded(true);
				}, 1000);
			}
		});
	};

	var contentLoading = function() {
		loadingWrap.className = 'loading';
	};

	var contentLoaded = function(firstflag) {
		initializeElm.className = 'hidden';
		if(!firstflag) { return; }
		loadingWrap.className = 'loading loaded';
		$('#workContainer').mixItUp();
	};

	var showLogo = function() {
		typeLego.className = 'visible';
	};

	$(document).ready(function(){

		initializeElm = document.getElementById('initialize');
		loadingWrap = document.getElementById('load');
		typeLego = document.getElementById('typeLogo');
	
		if(isVisited) {
			contentLoaded(false);
			return;
		} else {
			sessionStorage.setItem('imw-visited', true);

			showLogo();
			typing();
			contentLoading();
		}

	});

})(this, jQuery);