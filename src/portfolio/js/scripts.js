(function(window, $){
	"use strict";

	var CUSTOM_UTIL = {};
	CUSTOM_UTIL.isTouch = ('ontouchstart' in window);
	CUSTOM_UTIL.action = {
		start: CUSTOM_UTIL.isTouch ? 'touchstart' : 'mousedown',
		move: CUSTOM_UTIL.isTouch ? 'touchmove' : 'mousemove',
		end: CUSTOM_UTIL.isTouch ? 'touchend' : 'mouseup'
	};

	window.CUSTOM_UTIL = CUSTOM_UTIL;

	window.ClickClass = {};

	ClickClass = function(fn, cont) {
		var that = this,
			isTouch = false;
		var ACTION = CUSTOM_UTIL.action;

		var duration = CUSTOM_UTIL.isTouch ? 220 : 0;

		that.on(ACTION.start, function(e) {
			isTouch = true;
			that.addClass('tap');
		});
		that.on(ACTION.move, function(e) {
			if($(e.target).get(0) == that.get(0)) {
				e.preventDefault();
			}
			// 即キャンセル回避
			setTimeout(function() {
				isTouch = false;
				that.removeClass('tap');
			}, duration);
		});
		that.on(ACTION.end, function(e) {
			if(isTouch && $(e.target).get(0) == that.get(0)) {
				e.preventDefault();
				fn.apply(cont);
				that.removeClass('tap');
			}
		});
	};

})(this, jQuery);





(function(window, document) {
	"use strict";

	var contentLoaded = function() {
		var loadingWrap = document.getElementById('load');
		loadingWrap.className = 'loaded';
	}

	window.addEventListener('load', function() {
		var initialize = document.getElementById('initialize');

		if(!initialize) {
			contentLoaded();
		}
	}, false);

	window.contentLoaded = contentLoaded;

})(this, this.document);


(function(window, document) {
	"use strict";

	var heightOffset = 105; // 要素の高さ微調整

	var TrackElement = function(elm, wrapElm) {
		this.elm = elm;
		this.wrapElm = wrapElm;

		this.pageTop; // windowのページ上からの位置
		this.statusElm; // false(static), fixed, absolute

		this.rectTop; // 要素のページ上からの位置
		this.elmPageTop; // 要素のページ上からの絶対位置
		this.elmOffsetHeight; // 要素の高さ

		this.wrapRectTop; // 親要素のページ上からの位置
		this.wrapElmPageTop; // 親要素のページ上からの絶対位置
		this.wrapElmPageTopOffsetHeight; // 親要素の高さを含むページ上からの絶対位置

		this.positionBottomYAmount; // ボトム位置到着時の位置

		this.init();
	};

	TrackElement.prototype = {
		init: function() {
			var that = this;
			var rect = this.elm.getBoundingClientRect();
			var wrapRect = this.wrapElm.getBoundingClientRect();

			this.pageTop = window.pageYOffset;
			this.statusElm = false;

			this.rectTop = rect.top;
			this.elmPageTop = this.rectTop + this.pageTop;
			this.elmOffsetHeight = this.elm.offsetHeight + heightOffset;

			this.wrapRectTop = wrapRect.top;
			this.wrapElmPageTop = this.wrapRectTop + this.pageTop;
			this.wrapElmPageTopOffsetHeight = this.wrapElmPageTop + this.wrapElm.offsetHeight;

			this.positionBottomYAmount = this.wrapElmPageTopOffsetHeight - this.elmOffsetHeight - this.elmPageTop;

			window.addEventListener('scroll', function() {
				that.statusElm = that.getFixedStatus();

				that.changeElmStatus(that.statusElm);
			}, false);
		},
		getScrollAmount: function() {
			var amount = document.body.scrollTop || document.documentElement.scrollTop;
			return amount;
		},
		getFixedStatus: function() {
			var scrollAmount = this.getScrollAmount();
			var isReachTop = (scrollAmount >= this.elmPageTop) ? true : false;
			var isReachBottom = (scrollAmount + this.elmOffsetHeight >= this.wrapElmPageTopOffsetHeight) ? true : false;

			if (isReachTop) {
				if (!isReachBottom) {
					return "fixed";
				} else {
					return "absolute";
				}
			} else {
				return false;
			}
		},
		changeElmStatus: function(st) {
			if (st == "fixed") {
				this.elm.style.position = "fixed";
				this.elm.style.top = "0px";
			} else if (st == "absolute") {
				this.elm.style.position = "";
				this.elm.style.top = this.positionBottomYAmount + "px";
			} else {
				this.elm.style.position = "";
				this.elm.style.top= "0px";
			}
		}
	};

	window.TrackElement = TrackElement;

})(this, this.document);

