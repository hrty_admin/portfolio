/* ********************
// IE8以下非対象
// ****************** */

if(typeof window.addEventListener == "undefined" && typeof document.getElementsByClassName == "undefined"){

	var newElem = document.createElement("div");
	newElem.innerHTML = '<div style="padding: 35px 0;background: #fff;text-align: center;"><div class="post-area" style="margin-bottom: 0 !important;"><p>申し訳ありません。<br />現在ご利用中のInternet Explorerは、バージョンが古いため当サイトを閲覧することができません。<br />恐れ入りますが、ブラウザの無料バージョンアップ、もしくはGoogle Chromeをダウンロードしていただき、<br />再度閲覧していただければ幸いです。</p><p><a href="http://www.google.com/chrome/intl/ja/landing.html" target="_blank" class="blank">Google Chromeのダウンロードはこちら</a><br /><a href="http://www.microsoft.com/ja-jp/windows/products/winfamily/ie/function/default.aspx" target="_blank" class="blank">最新版のInternet Explorerのダウンロードはこちら</a></p></div></div>';
	document.body.insertBefore(newElem, document.body.firstChild);

}