'use strict';

var path = require('path');

var root = '/portfolio';

module.exports = function (grunt) {
	grunt.initConfig({
		// 作業フォルダ、公開&確認フォルダ設定
		connectPaths: {
			srcDir: './src',
			distDir:  './dist',
			buildDir:  './build'
		},
		paths: {
			srcDir: './src' + root,
			distDir:  './dist' + root,
			buildDir:  './build' + root
		},

		// config.rbの設定を記述
		compass: {
			dev: {
				options: {
	                httpPath: '/',
	                cssDir: '<%= paths.distDir %>/css/',
	                sassDir: '<%= paths.srcDir %>/scss/',
	                imagesDir: '<%= paths.srcDir %>/image/',
	                httpGeneratedImagesPath:  '../image/',
	                javascriptDir: '<%= paths.distDir %>/js/',
	                environment: 'development',
	                cache: false
				}
			}
		},

		copy: {
			js: {
				expand: true,
				dot: true,
				cwd: '<%= paths.srcDir %>/js/',
				dest: '<%= paths.distDir %>/js/',
				src: [
				  '**/*.*',
				]
			},
			img: {
				expand: true,
				dot: true,
				cwd: '<%= paths.srcDir %>/image/',
				dest: '<%= paths.distDir %>/image/',
				src: [
				  '**/*.{gif,jpeg,jpg,png,svg,webp,ico}'
				]
			},
			font: {
				expand: true,
				dot: true,
				cwd: '<%= paths.srcDir %>/font/',
				dest: '<%= paths.distDir %>/font/',
				src: [
				  '**/*.{eot,svg,ttf,woff}'
				]
			},
			json: {
				expand: true,
				dot: true,
				cwd: '<%= paths.srcDir %>/json/',
				dest: '<%= paths.distDir %>/json/',
				src: [
				  '**/*.json'
				]
			},
			build: {
				expand: true,
				dot: true,
				cwd: '<%= paths.distDir %>/',
				dest: '<%= paths.buildDir %>/',
				src: [
				  '**/*.*'
				]
			}
		},

		useminPrepare: {
			options: {
				root: "<%= connectPaths.distDir %>",
				dest: "build"
			},
			html: ["<%= connectPaths.buildDir %>/**/*.html"]
		},
		usemin: {
			options: {
				dirs: ["<%= connectPaths.buildDir %>/"]
			},
			html: ["<%= connectPaths.buildDir %>/**/*.html"]
		},

        // jadeの設定
        jade: {
            compile: {
                options: {
                	// インデント含む
                    pretty: true,
                	// config.jsonからサイト設定を読み込む
                    data: function(dest) {
                    	var confJson = require('./src/portfolio/jade/json/config.json');
                		// ファイル名をconfJson.fileNameについか
                    	confJson.fileName = dest;
                    	return {
                    		conf: confJson
                    	};
                    }
                },
                files: [{
                    expand: true,
                	// jadeファイルディレクトリ
                    cwd: '<%= paths.srcDir %>/jade',
                    src: ['**/!(_)*.jade'],
                	// コンパイル先ディレクトリ
                    dest: '<%= paths.distDir %>/',
                    ext: ['.html']
                }]
            }
        },

        connect: {
            dist: {
                options: {
                    port: 9000,
                    base: '<%= connectPaths.distDir %>/'
                }
            },
            build: {
                options: {
                    port: 9001,
                    base: '<%= connectPaths.buildDir %>/',
                    keepalive: true
                }
            }
        },

		esteWatch: {
			options: {
				livereload: {
					enabled: false
				},
				dirs: [
					'<%= paths.srcDir %>/jade/**/',
					'<%= paths.srcDir %>/scss/**/',
					'<%= paths.srcDir %>/image/sprite/',
					'<%= paths.srcDir %>/json/**/',
					'<%= paths.srcDir %>/js/**/'
				]
			},
			jade: function(filepath) {
				return ['newer:jade'];
			},
			scss: function(filepath) {
				return ['compass'];
			},
			spriteimg: function(filepath) {
				return ['newer:copy:image'];
			},
			json: function(filepath) {
				return ['newer:copy:json'];
			},
			js: function(filepath) {
				return ['newer:copy:js'];
			}
		},

	    // png圧縮
		pngmin: {
			compile: {
				options: {
					ext: '.png',
					force: true
				},
				files: [{
					expand: true,
					src: ['**/*.png'],
					cwd: '<%= paths.buildDir %>/image/',
					dest: '<%= paths.buildDir %>/image/'
				}]
			}
		},

		// jpg圧縮
		// imagemin: {
		// 	// まとめて
		// 	dynamic: {
		// 		files: [{
		// 			expand: true,
		// 			src: ['**/*.{jpg,gif}'],
		// 			cwd: '<%= paths.buildDir %>/image/',
		// 			dest: '<%= paths.buildDir %>/image/'
		// 		}
		// 	]}
		// },

		// jpg圧縮
		imageoptim: {
			// まとめて
			dynamic: {
				options: {
					jpegMini: false,
					imageAlpha: false,
					quitAfter: true
				},
				src: ["<%= paths.buildDir %>/image/**/*.jpg"]
			}
		},

		//ファイルの削除
		clean:{
			delDist: ["<%= paths.distDir %>"],
			delBuild: ["<%= paths.buildDir %>"]
		}

	});

	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-bower-task');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-este-watch');
	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-usemin');
	grunt.loadNpmTasks('grunt-pngmin');
	// grunt.loadNpmTasks('grunt-contrib-imagemin');
	// grunt.loadNpmTasks('grunt-imageoptim');
	
	grunt.registerTask('default', ['connect:dist', 'esteWatch']);
	grunt.registerTask('compile', ['clean:delDist', 'compass', 'copy:js', 'copy:img', 'copy:font', 'copy:json', 'jade', 'connect:dist', 'esteWatch']);
	// grunt.registerTask('build', ['clean:delBuild','compass', 'copy:build', 'useminPrepare', 'concat', 'uglify', 'cssmin', 'usemin', 'imageoptim', 'pngmin', 'connect:build']);
	grunt.registerTask('build', ['clean:delBuild','compass', 'copy:build', 'useminPrepare', 'concat', 'uglify', 'cssmin', 'usemin', 'pngmin', 'connect:build']);
}