(function(){
	"use strict";

	var data = [
		{
			label: "HTML / CSS",
			value: 40,
			highlight: "#75AAD9",
			color:"#5B90BF"
		},
		{
			label: "JavaScript",
			value: 22,
			highlight: "#B0CFCE",
			color: "#96B5B4"
		},
		{
			label: "CMS",
			value: 20,
			highlight: "#BDD8A6",
			color: "#A3BE8C"
		},
		{
			label: "API",
			value: 10,
			highlight: "#C59381",
			color: "#AB7967"
		},
		{
			label: "Design",
			value: 6,
			highlight: "#EAA18A",
			color: "#D08770"
		},
		{
			label: "Swift",
			value: 2,
			highlight: "#CEA8C7",
			color: "#B48EAD"
		}
	];
	var ctx = $("#skillChart").get(0).getContext("2d");
	var skillChart = new Chart(ctx).Doughnut(data, {
	    //Boolean - Whether we should show a stroke on each segment
	    segmentShowStroke : true,
	    scaleLineWidth: 1,
	    //Number - The percentage of the chart that we cut out of the middle
	    percentageInnerCutout : 0, // This is 0 for Pie charts
	    //Number - Amount of animation steps
	    animationSteps : 80,
	    //String - Animation easing effect
	    animationEasing : "easeInOutQuad",
	    
	    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",

		onAnimationComplete: function() {
			var $graphLegend = $("#graphLegend");

			$graphLegend.prepend(this.generateLegend()).fadeIn();

			$('li', $graphLegend).each(function() {
				setHoverEvent($(this));
			});

			$('.skill-item').each(function() {
				setHoverEvent($(this));
			});
		}
	});

	var setHoverEvent = function($e) {
		var segment;
		var segmentFillColor;
		$e.hover(function() {
			segment = skillChart.segments[$e.index()];

			segmentFillColor = segment.fillColor;
			segment.fillColor = segment.highlightColor;
			skillChart.showTooltip([segment]);
		}, function() {
			if(!segment){ return; }
			segment.fillColor = segmentFillColor;
			skillChart.showTooltip([]);
		});
	};

	document.addEventListener('DOMContentLoaded', function() {
		var tgt = document.getElementById('fix-skill-chart'),
			wrap = document.getElementById('fix-wrap');
		var trackElement = new TrackElement(tgt, wrap);
	}, false);


})();

(function(window, document, $){
	"use strict";

	var biography = document.getElementById('biography');

	$.getJSON("./json/biography.json", function(data) {
		var _l = data.length,
			_content = "";

		for(var i = 0; i < _l; i++) {
			_content += "<li><p class='timeline-date'>" + data[i].date + "</p><div class='timeline-content'><h3>" + data[i].title + "</h3><p>" + data[i].content + "</p></div></li>";
		}

		biography.innerHTML = _content;
	});

})(this, this.document, jQuery);

